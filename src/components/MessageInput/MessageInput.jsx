import React, { useState, useEffect } from 'react';

import './MessageInput.css';

const MessageInput = ({ sendMessage, message }) => {
  const [text, setText] = useState('');

  useEffect(() => {
    console.log('use effect');
    setText(text === '' ? message.text : text);
  }, [message.text, text]);

  const submit = () => {
    if (text === '') {
      return;
    }
    sendMessage({ ...message, text });
    setText('');
  };

  const handleKeyDown = event => {
    if (event.key !== 'Enter') {
      return;
    }
    submit();
  };

  return (
    <div className="message_input">
      <input placeholder="Type something..."
             onChange={ev => setText(ev.target.value)}
             value={text}
             onSubmit={() => console.log('submit')}
             onKeyDown={handleKeyDown}
      />
      <button onClick={submit}>Send</button>
    </div>
  );
};

export default MessageInput;