import React from 'react';
import Spinner from '../../components/Spinner/Spinner';

import './ChatDetails.css'

const ChatDetails = ({ users, media, files }) => {
  const firstUsers = users ? users.slice(0, 6) : [];
  const firstMedia = media ? media.slice(0, 6) : [];
  const firstFiles = files ? files.slice(0, 3) : [];

  return (
    <div className="chat_details">
      <img src="https://i.imgur.com/A0uXE7t.png" alt=""/>
      <span className="chat_description">Discussion of Binary Studio Academy</span>
      <span className="title">Group Members</span>
      {users
        ? <div className="members">
          {firstUsers.map(user => (
            <img src={user.url} alt=""/>
          ))}
        </div>
        : <Spinner />}
      <button className="view_all">View all ({users ? users.length : 0})</button>
      <span className="title">Photos & Multimedia</span>
      {media
        ? <div className="photos">
          {firstMedia.map(photo => (
            <img src={photo.url} alt=""/>
          ))}
        </div>
        : <Spinner />}
      <button className="view_all">View all ({media ? media.length : 0})</button>
      <span className="title">Attachments</span>
      <div className="attachments">
        <div className="file"><img src="https://img.icons8.com/fluent/48/000000/pdf.png" alt=""/><span>task-1.pdf</span></div>
        <div className="file"><img src="https://img.icons8.com/officel/30/000000/burn-cd.png" alt=""/><span>arch_linux.iso</span></div>
        <div className="file"><img src="https://img.icons8.com/dusk/64/000000/ms-powerpoint.png" alt=""/><span>why_bsa.pptx</span></div>
      </div>
      <button className="view_all">View all ({files ? files.length : 0})</button>
    </div>
  );
}

export default ChatDetails;