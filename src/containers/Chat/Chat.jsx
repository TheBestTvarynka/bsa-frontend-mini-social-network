import React, { useState } from 'react';
import Messenger from '../Messenger/Messanger';
import ChatDetails from '../ChatDetails/ChatDetails';
import Header from '../Header/Header';
import Spinner from '../../components/Spinner/Spinner';
import { v4 as uuidv4 } from 'uuid';

import { loadMessages, loadUsers, loadFiles, getCurrentUser, loadMedia, formatDate } from './actions';

import './Chat.css';

const Chat = () => {
  const [messages, setMessages] = useState(() => {
    loadMessages().then(data => setMessages(data));
    return undefined;
  });
  const [users, setUsers] = useState(() => {
    loadUsers().then(data => setUsers(data));
    return undefined;
  });
  const [media, setMedia] = useState(() => {
    loadMedia().then(data => setMedia(data));
    return undefined;
  });
  const [files, setFiles] = useState(() => {
    loadFiles().then(data => setFiles(data));
    return undefined;
  });
  const [tmpMessage, setTmpMessage] = useState({ text: '' });
  const [user] = useState(() => getCurrentUser());

  const addMessage = data => {
    if (data.id) {
      setMessages(messages.map(message => message.id === data.id ?
        { ...message, text: data.text, editedAt: new Date().toString() }
        : message));
      setTmpMessage({ text: '' });
    } else {
      const message = {
        id: uuidv4(),
        userId: user.id,
        avatar: user.avatar,
        user: user.user,
        text: data.text,
        createdAt: new Date().toString(),
        editedAt: ''
      };
      setMessages([...messages, message]);
    }
  }

  const deleteMessage = id => {
    setMessages(messages.filter(message => message.id !== id));
  }

  const updateMessage = (id, text) => {
    console.log('update message');
    console.log({ id, text });
    setTmpMessage({ id, text });
  };

  const likeMessage = (id, isLiked) => {
    setMessages(messages.map(message => message.id === id ? { ...message, isLiked }: message));
  };

  return (
    <div className="chat">
      <Header />
      {(!messages || !user)
        ? (
          <Spinner />
        ) : (
          <Messenger messages={messages}
                     sendMessage={addMessage}
                     deleteMessage={deleteMessage}
                     updateMessage={updateMessage}
                     likeMessage={likeMessage}
                     user={user}
                     inputData={tmpMessage}
          />
        )}
      <ChatDetails users={users} media={media} files={files}/>
    </div>
  );
}

export default Chat;