
export const loadMessages = async () => {
  const result = await fetch('https://thebesttravynka.github.io/chat-data/messages.json')
    .then(resp => resp.json());
  return result
    .map(message => ({ ...message, isLiked: false }));
};

export const loadUsers = async () => {
  const result = await fetch('https://thebesttravynka.github.io/chat-data/users.json')
    .then(resp => resp.json());
  return result;
}

export const loadMedia = async () => {
  const result = await fetch('https://thebesttravynka.github.io/chat-data/media.json')
    .then(resp => resp.json());
  return result;
}

export const loadFiles = async () => {
  const result = await fetch('https://thebesttravynka.github.io/chat-data/files.json')
    .then(resp => resp.json());
  return result;
};

export const getCurrentUser = () => {
  return {
    id: '747f5d55-e263-4d3a-8ee7-b3d9ab350dfd',
    user: 'Pavlo Myroniuk',
    avatar: 'https://i.imgur.com/ZBnR94f.png'
  };
};

export const formatDate = stringDate => {
  return new Date(stringDate.toString()).toLocaleTimeString();
};
