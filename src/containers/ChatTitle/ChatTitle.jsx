import React from 'react';

import './ChatTitle.css'

const ChatTitle = ({ lastMessageDate }) => {
  return (
    <div className="chat_title">
      <span className="chat_name">BSA UI/UX</span>
      <span className="last_message">last message at {new Date(lastMessageDate).toLocaleString()}</span>
    </div>
  );
}

export default ChatTitle;