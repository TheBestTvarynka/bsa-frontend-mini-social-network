import React from 'react';

import MessageList from '../MessageList/MessageList';
import MessageInput from '../../components/MessageInput/MessageInput';
import ChatTitle from '../ChatTitle/ChatTitle';

import './Messenger.css';

const Messenger = ({ messages, sendMessage, deleteMessage, updateMessage, likeMessage, user, inputData }) => {
  return (
    <div className="messenger">
      <ChatTitle lastMessageDate={messages[messages.length - 1].createdAt} />
      <MessageList messages={messages} user={user} deleteMessage={deleteMessage} updateMessage={updateMessage} likeMessage={likeMessage}/>
      <MessageInput sendMessage={sendMessage} message={inputData} />
    </div>
  );
}

export default Messenger;