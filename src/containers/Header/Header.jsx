import React, { useState } from 'react';

import './Header.css';

const Header = () => {
  return (
    <div className="header">
      <div className="app_name">
        <img src="https://i.imgur.com/hG4Th4U.jpg" />
        <span>AmazingChatApp</span>
      </div>
      <div className="user_profile">
        <img src="https://resizing.flixster.com/kr0IphfLGZqni5JOWDS2P1-zod4=/280x250/v1.cjs0OTQ2NztqOzE4NDk1OzEyMDA7MjgwOzI1MA" />
        <span>TheBestTvarynka</span>
      </div>
    </div>
  );
}

export default Header;